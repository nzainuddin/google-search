package com.demo.StepDefinitions;

import com.demo.config.Configuration;
import com.demo.pageObjects.GooglePage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

public class GoogleSteps {
    WebDriver driver;

    @Before
    public void setup() {
        System.out.println("Into the setup method of HelpStep...");
        this.driver = Configuration.setUp();
    }

    @After
    public void quit() {
        driver.quit();
        driver.close();
    }

    @Given("User navigate to {string} url")
    public void openUrl(String url) throws InterruptedException {
        GooglePage googlePage = new GooglePage(driver);
        googlePage.openWebUrl(url);
    }

    @When("Enter {string} on Google search input")
    public void searchOnGoogle(String input) throws InterruptedException {
        GooglePage googlePage = new GooglePage(driver);
        googlePage.googleMainPageDisplayed();
        googlePage.enterSearchInput(input);
    }

    @When("Select {string} search result")
    public void selectSearchResult(String searchResult) throws IOException {
        GooglePage googlePage = new GooglePage(driver);
        googlePage.clickSearchResult(searchResult);
    }

    @Then("Directed to selected search result url")
    public void directedToSelectedUrl() throws IOException {
        WebDriver driver = new ChromeDriver();
        GooglePage googlePage = new GooglePage(driver);
        googlePage.navigatedToSelectedLinkPage();
        driver.close();
    }

    @Then("Directed to {string} calculation result page")
    public void directedToCalcPage(String calculation) {
        WebDriver driver = new ChromeDriver();
        GooglePage googlePage = new GooglePage(driver);
        googlePage.directedToCalcPage(calculation);
        driver.close();
    }
}
