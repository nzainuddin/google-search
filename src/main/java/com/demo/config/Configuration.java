package com.demo.config;

import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Configuration {

        @Before
        public static WebDriver setUp() {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            System.getProperty("headless");
            WebDriver driver = new ChromeDriver();

            return driver;
        }

}
