Feature: Search functionality on Google

    Scenario: Able to search on Google | Select first result
        Given User navigate to "https://www.google.com" url
        When Enter "automation" on Google search input
        And Select "1st" search result
        Then Directed to selected search result url

    Scenario: Able to search on Google | Select random result
        Given User navigate to "https://www.google.com" url
        When Enter "automation" on Google search input
        And Select "random" search result
        Then Directed to selected search result url

    Scenario: Able to get calculation result on Google
        Given User navigate to "https://www.google.com" url
        When Enter "5+5" on Google search input
        Then Directed to "5+5" calculation result page