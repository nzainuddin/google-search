package com.demo.pageObjects;

import com.demo.utils.Base;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.util.Assert;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

public class GooglePage extends Base {
    WebDriver driver;
    String filePath = System.getProperty("user.dir") + "/src/test/resources/txtFile/search.txt";

    @FindBy(xpath = "//button[@aria-label='No thanks']")
    private WebElement noThanksButton;
    @FindBy(xpath = "//img[@alt='Google']")
    private WebElement googleImg;

    @FindBy(xpath = "//input[@aria-label='Search']")
    private WebElement searchBar;

    @FindBy(xpath = "//li/following-sibling::div//preceding-sibling::input[@value='Google Search']")
    private WebElement googleSearchBtn;

    @FindBy(xpath = "//div[@aria-label='About this Result']/ancestor::div/preceding-sibling::a")
    private List<WebElement> mainLink;

    public static final String searchedInput = "//input[@value='%s'][not(@type='hidden')]";

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    public void openWebUrl(String url) throws InterruptedException {
        openUrl(url);
    }

    public void googleMainPageDisplayed() {
        isDisplayed(googleImg);
    }

    public void enterSearchInput(String input) {
        sendKeys(searchBar, input);
        searchBar.sendKeys(Keys.RETURN);
    }

    public void clickSearchResult(String searchResult) throws IOException {
        Random random = new Random();
        int position;

        if (searchResult.equalsIgnoreCase("random")) {
           position = random.nextInt(mainLink.size() - 1) + 1;
        } else {
            // remove st, nd, th
            position = Integer.parseInt(searchResult.substring(0, searchResult.length()-2));
        }

        if (position < mainLink.size()) {
            writeUrlToFile(mainLink.get(position+1).getText());
            click(mainLink.get(position+1));
        } else {
            System.out.println("Out of range");
        }
    }

    public void navigatedToSelectedLinkPage() throws IOException {
        String currentUrl = getCurrURL();
        Assert.hasText(currentUrl, readFile());
    }

    public void directedToCalcPage(String input) {
        isDisplayed(searchedInput, input);
    }

    public void writeUrlToFile(String url) throws IOException {
        BufferedWriter bw;
        File file = new File(filePath);

        if(file.exists()) { file.delete(); }
        file.createNewFile();

        FileWriter writer = new FileWriter(file,true);
        bw = new BufferedWriter(writer);
        bw.write(url);
        bw.close();
        writer.close();
    }

    public String readFile() throws IOException {
        String data = new String(Files.readAllBytes(Paths.get(filePath)));
        return data;
    }
}
